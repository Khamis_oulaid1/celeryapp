
from flask import Flask, jsonify
from tasks import get_book
from celery import Celery
import time

app = Flask(__name__)
celery = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')

@app.route('/books')
def retrieve_book():
    # Call Celery task to retrieve book details asynchronously
    result = get_book.delay()
    book_details = result.get()  # Wait for task result
    if book_details:
        return jsonify(book_details), 200
    else:
        return 'Book not found', 404

if __name__ == '__main__':
    app.run(debug=True)

#comment









