import time

from celery import Celery

celery = Celery('tasks', broker='redis://localhost:6379/0', backend='redis://localhost:6379/0')

books = {
    1: {'title': 'The Great Gatsby', 'author': 'F. Scott Fitzgerald', 'price': 15.99},
    2: {'title': 'To Kill a Mockingbird', 'author': 'Harper Lee', 'price': 12.50},
    3: {'title': '1984', 'author': 'George Orwell', 'price': 10.75},
    4: {'title': 'Pride and Prejudice', 'author': 'Jane Austen', 'price': 14.25},
    5: {'title': 'The Catcher in the Rye', 'author': 'J.D. Salinger', 'price': 11.99}
}

@celery.task
def get_book():
    if books:
        time.sleep(5)
        return books
    else:
        return None



